#ifndef RESULT_CODES_H
#define RESULT_CODES_H

enum result_codes
{
	sOk,
	eCommandLineError,
	eFileError,
	eFail
};

#endif // RESULT_CODES_H