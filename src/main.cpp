#include "signature_calculator.h"
#include "result_codes.h"

#include <boost/program_options.hpp>

namespace detail
{
const int bytesInMb = 1024 * 1024;
} // namespace detail

int GetCommandLine(int argc, char* argv[], boost::program_options::variables_map& commandLine)
{
	try
	{
		namespace po = boost::program_options;
		po::options_description description("signature calculator options");
		description.add_options()
			("input_file", po::value<std::string>(), "set input file path")
			("output_file", po::value<std::string>(), "set output file path")
			("block_size", po::value<int>()->default_value(1), "block size in Mb");

		if (argc < 2)
		{
			std::cout << description << std::endl;
			return result_codes::eCommandLineError;
		}

		po::store(po::parse_command_line(argc, argv, description), commandLine);
		po::notify(commandLine);

		if (!commandLine.count("input_file"))
		{
			std::cout << "input file path should be set (--input_file)" << std::endl;
			return result_codes::eCommandLineError;
		}
		if (!commandLine.count("output_file"))
		{
			std::cout << "output file path should be set (--output_file)" << std::endl;
			return result_codes::eCommandLineError;
		}
	}
	catch (std::exception const& e)
	{
		std::cout << e.what() << std::endl;
		return result_codes::eCommandLineError;
	}
	return result_codes::sOk;
}

int main(int argc, char **argv)
{
	try
	{
		boost::program_options::variables_map commandLine;
		int res = GetCommandLine(argc, argv, commandLine);
		if (res != result_codes::sOk)
			return res;

		SignatureCalculator calculator;
		res = calculator.ProcessFile(commandLine["input_file"].as<std::string>(),
			commandLine["block_size"].as<int>() * detail::bytesInMb);
		if (res != result_codes::sOk)
			return res;

		std::vector<int> hashes = calculator.GetSignatures();
		std::ofstream outputFile(commandLine["output_file"].as<std::string>());
		for (int i = 0; i < hashes.size(); ++i)
			outputFile << hashes.at(i);

		std::vector<std::exception_ptr> exceptionPtrs = calculator.GetExceptionPtrs();
		if (!exceptionPtrs.empty()) 
		{
			for (int i = 0; i < exceptionPtrs.size(); ++i)
			{
				try
				{
					std::rethrow_exception(exceptionPtrs.at(i));
				}
				catch (std::exception const& e)
				{
					std::cout << "Thread exited with exception: " << e.what() << std::endl;
				}
			}
			return result_codes::eFileError;
		}
	}
	catch (std::exception const& e)
	{
		std::cout << e.what() << std::endl;
		return result_codes::eFail;
	}
	return result_codes::sOk;
}