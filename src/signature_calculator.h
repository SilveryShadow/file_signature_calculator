#ifndef SIGNATURE_CALCULATOR_H
#define SIGNATURE_CALCULATOR_H

#include <boost/asio/thread_pool.hpp>
#include <boost/asio/post.hpp>
#include <boost/crc.hpp>

#include <thread>
#include <fstream>
#include <iostream>
#include <string>
#include <atomic>
#include <stdexcept>
#include <condition_variable>

class SignatureCalculator
{
public:
	SignatureCalculator();
	~SignatureCalculator();

	int ProcessFile(const std::string& filePath, const int blockSize);
	void CalculateSignatures(const std::vector<char>& chunkData, const int chunk, const int blockSize);
	void CalculateSignature(const std::vector<char> blockData, int position);
	std::vector<int> GetSignatures();
	std::vector<std::exception_ptr> GetExceptionPtrs();

private:
	int m_numThreads;
	std::unique_ptr<boost::asio::thread_pool> m_pool;
	std::vector<int> m_resultSignatures;
	std::atomic_int m_signaturesCalculated = 0;
	int m_numBlocksInChunk = 0;
	int m_totalChunks = 0;
	std::condition_variable m_signaturesCalculationFinished;
	std::mutex m_mutex;
	std::atomic_bool m_notified = false;
	std::vector<std::exception_ptr> m_exceptionPtrs;
};

#endif // SIGNATURE_CALCULATOR_H