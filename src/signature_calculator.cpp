#include "signature_calculator.h"
#include "result_codes.h"

#include <boost/filesystem.hpp>

SignatureCalculator::SignatureCalculator()
{
	m_numThreads = std::thread::hardware_concurrency();
	if (!m_numThreads)
		m_numThreads = 4;

	m_pool = std::move(std::make_unique<boost::asio::thread_pool>(m_numThreads));
}

SignatureCalculator::~SignatureCalculator()
{
	m_pool->join();
}

int SignatureCalculator::ProcessFile(const std::string& filePath, const int blockSize)
{
	std::ifstream file(filePath);
	if (!file)
	{
		std::cout << "Error opening file " << filePath << std::endl;
		return result_codes::eFileError;
	}

	long long fileSize = boost::filesystem::file_size(filePath);
	if (fileSize == -1)
	{
		std::cout << "Cannot get file file " << filePath <<  " size" << std::endl;
		return result_codes::eFileError;
	}

	m_numBlocksInChunk = 2 * m_numThreads;

	const unsigned int chunkSize = blockSize * m_numBlocksInChunk;
	m_totalChunks = fileSize / chunkSize;
	unsigned int lastChunkSize = fileSize % chunkSize;

	if (lastChunkSize != 0)
		++m_totalChunks;
	else
		lastChunkSize = chunkSize;

	int totalBlocks = (m_totalChunks - 1) * m_numBlocksInChunk + lastChunkSize / blockSize;
	if (lastChunkSize % blockSize != 0)
		++totalBlocks;

	m_resultSignatures.resize(totalBlocks);
	for (int chunk = 0; chunk < m_totalChunks; ++chunk)
	{
		const int thisChunkSize = chunk == m_totalChunks - 1 ? lastChunkSize : chunkSize;

		std::vector<char> chunkData(thisChunkSize);
		file.read(&chunkData[0], thisChunkSize);

		CalculateSignatures(chunkData, chunk, blockSize);
	}
	return result_codes::sOk;
}

void SignatureCalculator::CalculateSignatures(const std::vector<char>& chunkData, const int chunk, const int blockSize)
{
	for (int block = 0; block < m_numBlocksInChunk; ++block)
	{
		const int startPosition = block * blockSize;
		const int resultPosition = chunk * m_numBlocksInChunk + block;
		if (chunk != m_totalChunks - 1)
		{
			std::vector<char> blockData(chunkData.begin() + startPosition,
				chunkData.begin() + startPosition + blockSize);
			boost::asio::post(*m_pool, std::bind(&SignatureCalculator::CalculateSignature, this, blockData, resultPosition));
		}
		else
		{
			if (startPosition < chunkData.size())
			{
				std::vector<char> blockData(chunkData.begin() + startPosition,
					chunkData.end());
				boost::asio::post(*m_pool, std::bind(&SignatureCalculator::CalculateSignature, this, blockData, resultPosition));
			}
		}
	}
}

void SignatureCalculator::CalculateSignature(const std::vector<char> blockData, int position)
{
	try
	{
		boost::crc_32_type result;
		result.process_bytes(blockData.data(), blockData.size());
		int hash = result.checksum();
		m_resultSignatures.at(position) = hash;
		m_signaturesCalculated++;

		if (m_signaturesCalculated.load() == m_resultSignatures.size())
		{
			std::unique_lock<std::mutex> lock(m_mutex);
			m_notified = true;
			m_signaturesCalculationFinished.notify_one();
		}
	}
	catch (std::exception const&)
	{
		std::unique_lock<std::mutex> lock(m_mutex);
		m_exceptionPtrs.push_back(std::current_exception());
		m_notified = true;
		m_signaturesCalculationFinished.notify_one();
	}
}

std::vector<int> SignatureCalculator::GetSignatures()
{
	std::unique_lock<std::mutex> lock(m_mutex);
	while (!m_notified.load())
		m_signaturesCalculationFinished.wait(lock);

	if (m_signaturesCalculated.load() != m_resultSignatures.size())
		return std::vector<int>();

	return m_resultSignatures;
}

std::vector<std::exception_ptr> SignatureCalculator::GetExceptionPtrs()
{
	return m_exceptionPtrs;
}